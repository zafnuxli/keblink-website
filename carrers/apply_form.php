<?php

/////// FILE UPLOAD //////////


$target_dir = "resume_uploads/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
  $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
  if($check !== false) {
    echo "File is an image - " . $check["mime"] . ".";
    $uploadOk = 1;
  } else {
    echo "File is not an image.";
    $uploadOk = 0;
  }
}

// Check if file already exists
if (file_exists($target_file)) {
  echo "Sorry, file already exists.";
  $uploadOk = 0;
}

// Check file size
if ($_FILES["fileToUpload"]["size"] > 500000) {
  echo "Sorry, your file is too large.";
  $uploadOk = 0;
}

// Allow certain file formats
if($imageFileType != "pdf" && $imageFileType != "doc" && $imageFileType != "docx"
&& $imageFileType != "jpg" && $imageFileType != "jpeg" && $imageFileType != "png" && $imageFileType != "odt") {
  echo "Les extensions autorisées sont .pdf, .doc, .docx, .odt, .jpeg, .jpg, .png.";
  $uploadOk = 0;
}

// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
  echo "Désolé, votre fichier n'a pas été uploadé.";
// if everything is ok, try to upload file
} else {
  if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
    echo "The file ". htmlspecialchars( basename( $_FILES["fileToUpload"]["name"])). " has been uploaded.";
  } else {
    echo "Sorry, there was an error uploading your file.";
  }
}


/////// FILE UPLOAD ///////



function spamcheck($field)
{
    //filter_var() sanitizes the e-mail
    //address using FILTER_SANITIZE_EMAIL
    $field=filter_var($field, FILTER_SANITIZE_EMAIL);

    //filter_var() validates the e-mail
    //address using FILTER_VALIDATE_EMAIL
    if(filter_var($field, FILTER_VALIDATE_EMAIL))
    {
        return true;
    }
    else
    {
        return false;
    }
}


function sendMail($toEmail, $fromEmail, $subject, $message, $headers)
{
    $validFromEmail = spamcheck($fromEmail);
    if($validFromEmail)
    {
        mail($toEmail, $subject, $message,  $headers);
        header('Location: http://keblink.com/index.html?success=1');

    }
}

$email = isset($_REQUEST['qemail']) ? $_REQUEST['qemail'] : false;

if($email != false)
{
    $to = "contact@keblink.com";
    $from = $_REQUEST['email'];
    $firstname = $_REQUEST['firstname'];
    $lastname = $_REQUEST['lastname'];
    $telephone = $_REQUEST['telephone'];
    $motivation = $_REQUEST['motivation'];

    $headers = "From: $from";
	$headers = "From: " . $from . "\r\n";
	$headers .= "Reply-To: ". $from . "\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=UTF-8\r\n";

    // $subject = "Vous avez un email de Keblink Contact.";

    $link = 'www.keblink.com';

	$body = "<!DOCTYPE html><html lang='en'><head><meta charset='UTF-8'><title>Une nouvelle requête</title></head><body>";
	$body .= "<table style='width: 100%;'>";
	$body .= "<thead style='text-align: center;'><tr><td style='border:none;' colspan='2'>";
	$body .= "<a href='http://keblink.com'><img src='http://keblink.com/img/logo.png' alt=''></a><br><br>";
	$body .= "</td></tr></thead><tbody><tr>";
	$body .= "<td style='border:none;'><strong>Nom:</strong> {$firstname} {$lastname}</td>";
	$body .= "<td style='border:none;'><strong>Email:</strong> {$from}</td>";
	$body .= "<td style='border:none;'><strong>Téléphone:</strong> {$telephone}</td>";
	$body .= "</tr>";
	$body .= "<tr><td style='border:none;'><strong>Objet: </strong>Une nouvelle candidature est arrivée</td></tr>";
	$body .= "<tr><td></td></tr>";
	$body .= "<tr><td colspan='2' style='border:none;'>{$motivation}</td></tr>";
	$body .= "</tbody></table>";
	$body .= "</body></html>";

    // $success = mail($to, $subject, $body, $headers);
    $success = sendMail($to, $email, $productType, $body, $headers);
}
?>
