<?php

function spamcheck($field)
{
    //filter_var() sanitizes the e-mail
    //address using FILTER_SANITIZE_EMAIL
    $field=filter_var($field, FILTER_SANITIZE_EMAIL);

    //filter_var() validates the e-mail
    //address using FILTER_VALIDATE_EMAIL
    if(filter_var($field, FILTER_VALIDATE_EMAIL))
    {
        return true;
    }
    else
    {
        return false;
    }
}


function sendMail($toEmail, $fromEmail, $subject, $message, $headers)
{
    $validFromEmail = spamcheck($fromEmail);
    if($validFromEmail)
    {
        mail($toEmail, $subject, $message,  $headers);
        header('Location: http://keblink.com/index.html?success=1');

    }
}

$email = isset($_REQUEST['qemail']) ? $_REQUEST['qemail'] : false;

if($email != false)
{
    $to = "contact@keblink.com";
    $from = $_REQUEST['qemail'];
    $productType = $_REQUEST['productType'];
    $name = $_REQUEST['qname'];
    $cmessage = $_REQUEST['qmessage'];

    $headers = "From: $from";
	$headers = "From: " . $from . "\r\n";
	$headers .= "Reply-To: ". $from . "\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=UTF-8\r\n";

    // $subject = "Vous avez un email de Keblink Contact.";

    $link = 'www.keblink.com';

	$body = "<!DOCTYPE html><html lang='en'><head><meta charset='UTF-8'><title>Une nouvelle requête</title></head><body>";
	$body .= "<table style='width: 100%;'>";
	$body .= "<thead style='text-align: center;'><tr><td style='border:none;' colspan='2'>";
	$body .= "<a href='http://keblink.com'><img src='http://keblink.com/img/logo.png' alt=''></a><br><br>";
	$body .= "</td></tr></thead><tbody><tr>";
	$body .= "<td style='border:none;'><strong>Nom:</strong> {$name}</td>";
	$body .= "<td style='border:none;'><strong>Email:</strong> {$from}</td>";
	$body .= "</tr>";
	$body .= "<tr><td style='border:none;'><strong>Objet: </strong>Devis sur {$productType}</td></tr>";
	$body .= "<tr><td></td></tr>";
	$body .= "<tr><td colspan='2' style='border:none;'>{$cmessage}</td></tr>";
	$body .= "</tbody></table>";
	$body .= "</body></html>";

    // $success = mail($to, $subject, $body, $headers);
    $success = sendMail($to, $email, $productType, $body, $headers);
}
?>
